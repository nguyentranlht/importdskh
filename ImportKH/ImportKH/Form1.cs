﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using OfficeOpenXml;
using Excel = Microsoft.Office.Interop.Excel;
using ImportKH.Models;
using System.Data.Entity;
using System.Linq.Expressions;

namespace ImportKH
{
    public partial class Form1 : Form
    {
        OpenFileDialog openFileDialog = new OpenFileDialog();
        public Form1()
        {
            InitializeComponent();
        }
        private void ImportExcel(string path)
        {
            CustomerDB context = new CustomerDB();
            var customer = context.Customers.ToList();

            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    ExcelPackage excelPackage = new ExcelPackage(new FileInfo(path));
                    ExcelWorksheet excelWorksheet = excelPackage.Workbook.Worksheets[0];
                    int endPoint = 2;
                    for (int i = excelWorksheet.Dimension.Start.Row + 1; i == endPoint; i++)
                    {
                        Customer tmp = new Customer();
                        tmp.CustomerID = excelWorksheet.Cells[i, 1].Value.ToString();
                        tmp.CustomerName = excelWorksheet.Cells[i, 2].Value.ToString();
                        tmp.Birthday = (DateTime)excelWorksheet.Cells[i, 3].Value;
                        tmp.PhoneNumber = excelWorksheet.Cells[i, 4].Value.ToString();
                        tmp.Email = excelWorksheet.Cells[i, 5].Value.ToString();
                        tmp.Address = excelWorksheet.Cells[i, 6].Value.ToString();

                        try
                        {
                            var result = customer.First(p => p.CustomerID.Trim() == tmp.CustomerID.ToString());
                            tmp.TT = "Cập nhật";
                            var index = customer.IndexOf(result);
                            customer[index] = tmp;
                        }
                        catch
                        {
                            tmp.TT = "Thêm mới";
                            customer.Add(tmp);
                        }
                        if (excelWorksheet.Cells[i + 1, 1].Value != null)
                        {
                            endPoint++;
                        }

                    }

                    context.SaveChanges();
                    transaction.Commit();
                    MessageBox.Show("Thêm dữ liệu thành công");
                }
                catch
                {
                    transaction.Rollback();
                    MessageBox.Show("Thêm dữ liệu thất bại");
                }
            }
        }


        private void btImport_Click(object sender, EventArgs e)
        {

            try
            {
                ImportExcel(txtFile.Text);
                CustomerDB context = new CustomerDB();
                var listCustomer = context.Customers.ToList();
                BindGrid(listCustomer);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void btAdd_Click(object sender, EventArgs e)
        {
            openFileDialog.Title = "Import Excel";
            openFileDialog.Filter = "Excel Files|*.xls;*.xlsx;*.xlsm";
            openFileDialog.ShowDialog();
            txtFile.Text = openFileDialog.FileName;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                CustomerDB context = new CustomerDB();
                var listCustomer = context.Customers.ToList();
                BindGrid(listCustomer);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void BindGrid(List<Customer> listCustomer)
        {
            dtgCustemer.Rows.Clear();
            foreach (var item in listCustomer)
            {
                int i = dtgCustemer.Rows.Add();
                dtgCustemer.Rows[i].Cells[0].Value = item.TT;
                dtgCustemer.Rows[i].Cells[1].Value = item.CustomerID;
                dtgCustemer.Rows[i].Cells[2].Value = item.CustomerName;
                DateTime dt = (DateTime)item.Birthday;
                string date = dt.ToString("dd/MM/yyyy");
                dtgCustemer.Rows[i].Cells[3].Value = date;
                dtgCustemer.Rows[i].Cells[4].Value = item.PhoneNumber;
                dtgCustemer.Rows[i].Cells[5].Value = item.Email;
                dtgCustemer.Rows[i].Cells[6].Value = item.Address;
            }
        }
    }
}
